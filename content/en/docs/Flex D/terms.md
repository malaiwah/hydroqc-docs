---
title: Termes
linkTitle: Termes
weight: 42
description: |
  Termes utilisés en lien avec les crédits hivernaux
lastmod: 2022-09-20T23:47:28.801Z
---

Les termes suivants sont utilisés dans ce module et proviennent autant que possible du document officiel des taux d'électricité hydro-quebec (page 37):

[https://www.hydroquebec.com/data/documents-donnees/pdf/electricity-rates.pdf](https://www.hydroquebec.com/data/documents-donnees/pdf/tarifs-electricite.pdf#page=41)

et du document de la “Régie de l’énergie” :

[http://publicsde.regie-energie.qc.ca/projets/469/DocPrj/R-4057-2018-B-0062-DDR-RepDDR-2018_10_26.pdf#page=124](http://publicsde.regie-energie.qc.ca/projets/469/DocPrj/R-4057-2018-B-0062-DDR-RepDDR-2018_10_26.pdf#page=124)

## Periode

Dans le contexte des crédits hivernaux, une période est une plage horaire pendant laquelle un tarif spécifique ou une logique algorythmique est appliquée par Hydro-Québec.

Il y a 2 types de période:

### Pointe

Une période de pointe est une période pendant laquelle la consommation d'énergie de tous les clients est généralement élevée.
Dans le contexte des crédits hivernaux, les plages horaire suivantes sont des périodes de pointe:

Période de pointe matinale : 06:00 à 09:00
Période de pointe du soir: 16:00 à 20:00

### Normale

Toute période qui n'est pas une une pointe.

