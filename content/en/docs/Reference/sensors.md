---
title: Sensors
linkTitle: Sensors
weight: 32
description: |
  HydroQC sensor description.
lastmod: 2022-12-21T20:27:36.262Z
---

## Sensor Description

|Default HA name | Values | Description |
|-|-|-|
| Balance | CA$123.43 | Your current account balance |
| Current billing period average temperature | 15°C | The average temperature for the current billing period as calculated by Hydro-Quebec |
| Current billing period duration | 61 | The total number of days in this billing period |
| Current billing period current day | 19 | The day we are in the current billing period (ex: day 19 of 61 total) |
| Current billing period total to date | CA$108.10 | The cost of your consumption for the period as of today |
| Current billing period total consumption | 1149 kWh | Current consumption for the period as of today |
| Current billing period projected bill | CA$318.15 | Project cost of consumption at the end of the billing period |
| Current billing period projected total consumption | 3,432 kWh | Projected consumption at the end of the billing period |
| Current billing period daily bill mean | CA$5.69 | Average cost per day for the period |
| Current billing period daily consumption mean | 60.5 kWh | Average consumption per day for the period |
| Current billing period kwh cost mean | CA$0.09 | Average cost
| Current billing period lower price consumption | 1149 kWh | Consumption in the lower price bracket |
| Current billing period higher price consumption | 0 | Consumption in the higher price bracket |
| Current period epp enabled | On/Off | If you are subscribed to Equal Payment Plan |
| Next or current outage | timestamp | Information on the next planned outage or the status or the current unplanned outage |