---
title: Blueprint for Winter Credits
linkTitle: Blueprint for Winter Credits
weight: 45
description: |
  Home-Assistant Blueprint for Winter Credits
lastmod: 2023-01-17T02:12:08.942Z
---

## Home-Assistant Blueprint

A Home-Assistant Blueprint with the options to carry out the automations described in this section is available [here](https://raw.githubusercontent.com/hydroqc/hass-blueprint-hydroqc/main/hydroqc-winter-credit.yaml) and can be installed directly with this button:

[![Open your Home Assistant instance and show the blueprint import dialog with a specific blueprint pre-filled.](https://my.home-assistant.io/badges/blueprint_import.svg)](https://my.home-assistant.io/redirect/blueprint_import/?blueprint_url=https%3A%2F%2Fraw.githubusercontent.com%2Fhydroqc%2Fhass-blueprint-hydroqc%2Fmain%2Fhydroqc-winter-credit.yaml)