---
title: Capteurs
linkTitle: Capteurs
weight: 32
description: |
  Description des capteurs présent dans Home-Assistant
lastmod: 2022-12-21T20:26:46.748Z
---

## Sensor Description

| Nom du capteur | Valeurs | Description |
|-|-|-|
| Balance | CA$123.43 | Le solde de votre compte actuel |
| Current billing period average temperature | 15°C | La température moyenne pour la période de facturation actuelle telle que calculée par Hydro-Québec |
| Current billing period duration | 61 | Le nombre total de jours dans cette période de facturation |
| Current billing period current day | 19 | Le jour où nous sommes dans la période de facturation actuelle (Ex: Jour 19 sur 61 au total) |
| Current billing period total to date | CA$108.10 | Le coût de votre consommation pour la période à ce jour |
| Current billing period total consumption | 1149 kWh | Consommation actuelle pour la période en date d'aujourd'hui |
| Current billing period projected bill | CA$318.15 | Coût de la consommation du projetée à la fin de la période de facturation |
| Current billing period projected total consumption | 3,432 kWh | Consommation projetée à la fin de la période de facturation |
| Current billing period daily bill mean | CA$5.69 | Coût moyen par jour pour la période |
| Current billing period daily consumption mean | 60.5 kWh | Consommation moyenne par jour pour la période |
| Current billing period kwh cost mean | CA$0.09 | Coût moyen
| Current billing period lower price consumption | 1149 kWh | Consommation dans la braquette de prix basse |
| Current billing period higher price consumption | 0 kWh | Consommation dans la braquette de prix élevée |
| Current period epp enabled | On/Off | Inscription au mode de versement égaux |
| Next or current outage | timestamp | Information sur la prochaine panne prévue ou panne imprévue en cours |