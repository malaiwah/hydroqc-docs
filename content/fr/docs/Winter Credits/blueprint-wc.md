---
title: Blueprint Crédits Hivernaux
linkTitle: Blueprint Crédits Hivernaux
weight: 45
description: |
  Blueprint Home-Assistant pour Crédits Hivernaux
lastmod: 2023-01-17T02:12:08.942Z
---

## Blueprint Home-Assistant

Un Blueprint Home-Assistant avec les options pour réaliser les automatismes décrits dans cette section est disponible [ici](https://raw.githubusercontent.com/hydroqc/hass-blueprint-hydroqc/main/hydroqc-winter-credit.yaml) et peux être installé directement avec le bouton suivant:

[![Open your Home Assistant instance and show the blueprint import dialog with a specific blueprint pre-filled.](https://my.home-assistant.io/badges/blueprint_import.svg)](https://my.home-assistant.io/redirect/blueprint_import/?blueprint_url=https%3A%2F%2Fraw.githubusercontent.com%2Fhydroqc%2Fhass-blueprint-hydroqc%2Fmain%2Fhydroqc-winter-credit.yaml)